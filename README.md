# Deprecation Warring! voZer is deprecated! Please, use the [reZov](https://github.com/Nediorg/reZov)


<h1 align="center">
  <img src="assets/logo.jpg" alt="voZer"><br>
</h1>

<p align="center">voZer is another Telegram bot, written in Python3, that can help you write random text from the bot's base (botbase.txt or [chatid].txt), which is updated with chat messages, and which, in the right base mode, is connected between chats where the bot is.</p>

## Demo

<img src="assets/demo.png" alt="User: Hi!
                                Bot: hi!
                                User: You are cool :)
                                Bot: yes
                                User: /ping
                                Bot: Pong!
                                User: /info
                                Bot: Bot version: 1.0.0 OFFICIAL by luneedev (thanks to others)
                                Platform: Linux 5.15.0-2-amd64
                                Python version: 3.9.9 (main, Dec 16 2021, 23:13:29) 
                                [GCC 11.2.0]">

## Installation

1. Clone repository via `git clone https://codeberg.org/voZer-project/voZer.git`
2. Install packages<br>
on Linux: `pip3 install -r requirements.txt`<br>
on Windows: `pip install -r requirements.txt`<br>
4. Write your config in [config.py](src/config.py)
5. Start bot via `python3 main.py`

