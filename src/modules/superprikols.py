# Locals
lorem_ipsum_text = 'Voluptatum et eos qui similique cum nobis. Accusamus aliquam qui quisquam voluptate voluptas. Sit non nihil voluptas aliquid. Corrupti quasi impedit dolores. Adipisci minima sit ut molestiae autem dignissimos sunt.\n\nIn sint velit autem rerum sint sit perferendis. Iusto officiis qui vel doloribus. Illo et atque impedit voluptatem.\n\nPerspiciatis velit facere sed porro consequatur distinctio saepe debitis. Perspiciatis et aut molestiae architecto veniam tenetur velit dolorem. Dolores iusto sunt doloremque enim tempora deleniti. Qui inventore aut quia eius nulla corrupti dolorem. Occaecati accusamus iste laborum autem sed rerum aut.\n\nAperiam explicabo amet fugit explicabo blanditiis cum blanditiis ut. Quia dicta voluptatem saepe non consequuntur sit. Distinctio culpa vero sunt quis atque assumenda corporis ut. Eos rerum quas rerum cupiditate atque molestiae sint.\n\nQuo consequatur expedita est. Eum asperiores ad repudiandae quaerat quos aut doloremque placeat. Est neque beatae ipsa saepe neque aspernatur cumque. Natus nemo molestiae rem autem. Debitis voluptatem voluptatem incidunt.'
bricker_warning = 'do nat instul dis briker'
ban_warning = 'What was wrong with you when you posted this? Delete it before the first admin in this chat comes in.'

@dp.message_handler(commands='lipsum')
async def lorem_ipsum(message: types.Message):
    if check_bl_wl():
        await message.reply(lorem_ipsum_text)
    else:
        pass

@dp.message_handler(commands='wtf')
async def wtf_report(message: types.Message):
    if message.chat.id != message.from_user.id:
        await message.reply_to_message.reply(ban_warning)

@dp.message_handler(content_types='file')
async def bricker_warn(message: types.Message):
    await message.reply(bricker_warning)
