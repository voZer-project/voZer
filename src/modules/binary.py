import re

async def text_to_bits(text, encoding='utf-8', errors='surrogatepass'):
    bits = bin(int.from_bytes(text.encode(encoding, errors), 'big'))[2:]
    return ' '.join(re.findall('.{%s}' % 8, str(bits.zfill(8 * ((len(bits) + 7) // 8)))))

async def text_from_bits(bits, encoding='utf-8', errors='surrogatepass'):
    n = int(bits.replace(' ', ''), 2)
    return n.to_bytes((n.bit_length() + 7) // 8, 'big').decode(encoding, errors) or '\0'

@dp.message_handler(commands='tobin')
async def convert_to_binary(message: types.Message):
    await message.reply(await text_to_bits(message.text[message.entities[0].length:]))

@dp.message_handler(commands='frombin')
async def convert_from_binary(message: types.Message):
    text = await text_from_bits(message.text[message.entities[0].length:])
    if 't.me/+42777' not in text:
        await message.reply(text)
    else:
        await message.reply('Please don`t try to get me banned. First, you need to do your homework')
