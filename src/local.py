not_whitelisted_msg = '<b>🚫 Access to the bot is denied, you/this chat is off the whitelist.</b>'
blacklisted_msg = '<b>🚫 Access to the bot is denied, you/this chat is blacklisted.</b>'
hello_msg = 'Hi! This is voZer, a bot that generates one whole chunk of text from chat messages. It works based on a Markov chain.\n\nWARNING: We are not responsible for what the bot says or generates.'
func_button = 'List of commands'
func_info = '''/bessrom
/tobin
/frombin
/enable
/disable
/botonoff
/logsmode
/basemode
/broadcast (for admins)
/justsay
/saymuch
/stupidjoke
/bash
/getbotbase
/getchatbase
/info
/stats
/dopeaphoto
/dem
/jpeg
/random
/lipsum
/wtf
/tts
/comic
/wttr
/wfull
/s
'''
contact_with_admin_text = 'Contact with admin'
user_msg_title = 'User message:\n'
bot_answer_title = 'Bot answer:\n'
splash = '''
██    ██ ███████ ███████ ███████ ██████  
██    ██ ██   ██      ██ ██      ██   ██ 
 ██  ██  ██   ██   ███   █████   ██████ 
  ████   ██   ██ ██      ██      ██   ██ 
   ██    ███████ ███████ ███████ ██   ██ '''
okay_msg = 'OK'
prviacy_policy_title = 'Privacy Policy'
privacy_policy_text = """This bot's Privacy Policy:
1. We collect: text messages in chats where the bot is used, Telegram IDs of bot users, specified in Telegram user names, Telegram IDs of chats that use the bot.
2. We do not send or sell this data anywhere.
3. This data is stored only on the server where this bot works.
4. This is necessary for the bot to work and keep statistics on users.
5. When you using this bot, you agree to our Privacy Policy.
6. If you want to delete ALL your data from this bot, feel free to contact the admin."""
yes = 'Yes'
no = 'No'
wrong = 'Wrong usage.'
change_info_error = 'Sorry, but you <b>cannot</b> change group info.'
