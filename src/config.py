token = ''

min_random = 0
max_random = 1
random_result = 1

# If 0 or other, whitelist will be enabled
blacklist = 1

# PLZ DO NOT SAVE DEFAULT SETTINGS HERE, SET YOURS
news_channel = 'zprojectch'
admin_username = 'bootloop'
admin_id = 572558342
admins = [572558342]
logs_channel = 'zlogs'
logs_channel_id = -1001653885173

modules_dir = 'modules/'

stats_dir = 'Stats/'
path_to_num_of_all_gen_msgs = 'Stats/num_of_all_gen_msgs.txt'

path_to_base = 'Bases/botbase.txt'
path_to_log = 'Logs/log_{time}.log'

lists_dir = 'Lists/'
lists_files = ['chatslist', 'blacklist.txt', 'whitelist.txt', 'botbasechats.txt', 'disabledchats.txt', 'logsdisabledchats.txt']

levels = [0, 100, 250, 500, 1000]
level_names = ['newbie', 'typical voZer user', 'voZer active user', 'voZer king', 'voZer god', '...']
